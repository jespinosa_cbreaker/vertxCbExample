# Vertx example with circuit breaker

#### 1. Setup kafka Download kafka version 0.10.1.0
  - Run this command to start zookeeper:
    > bin/zookeeper-server-start.sh config/zookeeper.properties

  - Run this command in a new cmd to start kafka server:
    > bin/kafka-server-start.sh config/server.properties

  - Run this command to start a consumer for tracking topic:
    > bin/kafka-console-consumer.sh --zookeeper localhost:2181 --topic tracking

  - Run this command to produce some data:
    > bin/kafka-console-producer.sh --broker-list localhost:9092 --topic tracking

  - If u verify we get data in the consumer, installation is ok so we can stop producer


#### 2. Download hystrix dashboard and run it:
     > git clone https://github.com/Netflix/Hystrix.git
     > cd Hystrix/hystrix-dashboard
     > ../gradlew appRun

#### 3. Checkout example code and run application from intellij:
  - Main class: io.vertx.core.Launcher
  - Program arguments: run com.jespinosa.example.Server



#### 4. test the app:
  - Call hystrix dashboard url: http://localhost:7979/hystrix-dashboard
    - Config hystrix url with http://localhost:8081/vertx/hystrix-metrics
    - Fill in title and click on Add Stream
    - Click in Monitor Streams
  - Call endpoint http://localhost:8081/vertx/test?user=jose to generate data
  - Verify u receive data in the consumer
  - Verify u can see the calls in dashboard
