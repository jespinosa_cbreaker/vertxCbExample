package com.jespinosa.example;

import io.vertx.circuitbreaker.HystrixMetricHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;

import java.util.Date;

public class Server extends AbstractVerticle {

    public static final String PRODUCED_TYPE_JSON = "application/json;charset=utf-8";

    @Override
    public void start() throws Exception {

        SenderService senderService = new SenderService(vertx);
        Router router = Router.router(vertx);

        router.get("/vertx/hystrix-metrics").handler(HystrixMetricHandler.create(vertx));

        router.get("/vertx/test").handler(routingContext -> {

            String user = routingContext.request().getParam("user");
            Integer n = senderService.getNumber();

            String json = "{\"number\": " + n + ", \"user\": \"" + user + "\", \"time\": \"" + new Date().getTime() + "\"}";

            senderService.produce(json);

            HttpServerResponse response = routingContext.response();
            response.putHeader("content-type", PRODUCED_TYPE_JSON);
            response.setChunked(true);
            response.write(json).end();
        });

        vertx.createHttpServer().requestHandler(router::accept).listen(8081);
    }

}
